import { defineStore } from 'pinia';
import { getCheckoutInfo } from '@/apis/checkout'

export const useCheckoutStore = defineStore('checkout', {
    state: () => ({
        checkInfo: {},  // 订单对象
        curAddress: {}, // 地址对象
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async getCheckoutData() {
            let res = await getCheckoutInfo()
            // console.log("getCheckoutData ~ res:", res)
            this.checkInfo = res.result
            this.curAddress = this.checkInfo.userAddresses.find(item => item.isDefault === 0)
        },
        // 切换地址
        changeAddress(address) {
            this.curAddress = address
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});