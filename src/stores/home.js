import { defineStore } from 'pinia';
import { getBanner, getNew, getHot, getGoods } from '@/apis/home'

export const useHomeStore = defineStore('home', {
    state: () => ({
        bannerList: [],  // banner
        newList: [],   // 新鲜好物
        hotList: [],  // 人气推荐
        goodsList: [],  // 产品列表
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async getBannerList() {
            let res = await getBanner()
            // console.log("getBannerList ~ res:", res)
            this.bannerList = res.result
        },
        async getNewList() {
            let res = await getNew()
            // console.log("getNewList ~ res:", res)
            this.newList = res.result
        },
        async getHotList() {
            let res = await getHot()
            // console.log("getHotList ~ res:", res)
            this.hotList = res.result
        },
        async getGoodsList() {
            let res = await getGoods()
            // console.log("getGoodsList ~ res:", res)
            this.goodsList = res.result
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});