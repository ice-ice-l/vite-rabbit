import { defineStore } from 'pinia';
import { login, getLikeList, getUserOrder } from '@/apis/user'

export const useUserStore = defineStore('user', {
    state: () => ({
        userInfo: {}, // 用户信息
        likeList: [], // 猜你喜欢
        orderList: [], // 订单列表
        total: 0,  // // 补充总条数
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async toLogin(data) {
            let res = await login(data)
            // console.log("toLogin ~ res:", res)
            ElMessage({ type: 'success', message: '登录成功' })
            this.userInfo = res.result
        },
        logOut() {
            this.userInfo = {}
        },
        // 猜你喜欢
        async getLikeLists(data) {
            let res = await getLikeList(data)
            // console.log("getLikeLists ~ res:", res)
            this.likeList = res.result
        },
        // 订单
        async getUserOrderList(data) {
            let res = await getUserOrder(data)
            // console.log("getLikeLists ~ res:", res)
            this.orderList = res.result.items
            this.total = res.result.counts
        }
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});