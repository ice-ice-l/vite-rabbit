import { defineStore } from 'pinia';

export const useSetStore = defineStore('set', {
    state: () => ({
        count: 1,
    }),
    getters: {
        doubleCount: (state) => state.count * 2,
    },
    actions: {
        increment() {
            this.count++;
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});