import { defineStore } from 'pinia';
import { createOrder, getOrder } from '@/apis/pay'

export const usePayStore = defineStore('pay', {
    state: () => ({
        orderId: '',  // 订单id
        orderInfo: {}, // 支付信息
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async handerCreateOrder(data) {
            let res = await createOrder(data)
            // console.log("hanerCreateOrder ~ res:", res)
            this.orderId = res.result.id
        },
        async getOrderInfo(id) {
            let res = await getOrder(id)
            // console.log("getOrderData ~ res:", res)
            this.orderInfo = res.result
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});