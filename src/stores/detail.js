import { defineStore } from 'pinia';
import { getDetail, getHotGoods } from '@/apis/detail'
import Const from '@/utils/const';

export const useDetailStore = defineStore('detail', {
    state: () => ({
        detail: {},  // 详情
        hotTimeGoods: [],  // 24小时热销榜
        hotWeekGoods: [], // 周热销榜
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async getDetailData(id) {
            let res = await getDetail(id)
            // console.log("getDetailData ~ res:", res)
            this.detail = res.result
        },
        async getHotGoodsData(data) {
            let res = await getHotGoods(data)
            // console.log("getHotGoodsData ~ res:", res)
            switch (data.type) {
                case Const.HOT_TITLE.TIME:
                    this.hotTimeGoods = res.result
                    break;
                case Const.HOT_TITLE.WEEK:
                    this.hotWeekGoods = res.result
                    break;
                default:
                    break;
            }
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});