import { defineStore } from 'pinia';
import { getCategory } from '@/apis/layout'
import { getTopCategory } from '@/apis/category'
import { getBanner } from '@/apis/home'

export const useCategoryStore = defineStore('category', {
    state: () => ({
        categoryList: [],
        topCategoryInfo: {}, // 一级分类
        bannerList: [],  // banner
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async getCategoryList() {
            let res = await getCategory()
            // console.log("getCategoryList ~ res:", res)
            this.categoryList = res.result
        },
        async getTopCategoryInfo(id) {
            let res = await getTopCategory(id)
            // console.log("getTopCategoryList ~ res:", res)
            this.topCategoryInfo = res.result
        },
        async getBannerList(params) {
            let res = await getBanner(params)
            // console.log("getBannerList ~ res:", res)
            this.bannerList = res.result
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});