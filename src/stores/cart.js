import { defineStore } from 'pinia';
import { insertCart, deleteCart, findNewCartList, mergeCart } from '@/apis/cart'
import { useUserStore } from './user'

export const useCartStore = defineStore('cart', {
    state: () => ({
        cartList: [], // 购物车
    }),
    getters: {
        // 1. 总的数量 所有项的count之和
        allCount: (state) => state.cartList.reduce((a, c) => a + c.count, 0),
        // 2. 总价 所有项的count*price之和
        allPrice: (state) => state.cartList.reduce((a, c) => a + c.count * c.price, 0),
        // 3. 已选择数量
        selectedCount: (state) => state.cartList.filter(item => item.selected).reduce((a, c) => a + c.count, 0),
        // 4. 已选择商品价钱合计
        selectedPrice: (state) => state.cartList.filter(item => item.selected).reduce((a, c) => a + c.count * c.price, 0),

        // 是否全选计算属性
        isAll: (state) => state.cartList.every((item) => item.selected),
    },
    actions: {
        // 获取最新购物车列表action
        async updateNewList() {
            const res = await findNewCartList()
            // console.log("updateNewList ~ res:", res)
            this.cartList = res.result
        },
        // 添加购物车
        async addCart(goods) {
            console.log('添加', goods)
            const { skuId, count } = goods

            // 是否登录
            const userStore = useUserStore()
            let isLogin = userStore?.userInfo?.token
            if (isLogin) {
                // 登录之后的加入购车逻辑
                await insertCart({ skuId, count })
                this.updateNewList()
            } else {
                // 添加购物车操作
                // 已添加过 - count + 1
                // 没有添加过 - 直接push
                // 思路：通过匹配传递过来的商品对象中的skuId能不能在cartList中找到，找到了就是添加过
                const item = this.cartList.find((item) => goods.skuId === item.skuId)
                if (item) {
                    // 找到了
                    item.count += goods.count
                } else {
                    // 没找到
                    this.cartList.push(goods)
                }
            }
            ElMessage({ type: 'success', message: '添加成功' })
        },
        // 删除购物车
        async delCart(skuId) {
            // 是否登录
            const userStore = useUserStore()
            let isLogin = userStore?.userInfo?.token
            if (isLogin) {
                // 调用接口实现接口购物车中的删除功能
                await deleteCart([skuId])
                this.updateNewList()
            } else {
                // 思路：
                // 1. 找到要删除项的下标值 - splice
                // 2. 使用数组的过滤方法 - filter
                const idx = this.cartList.findIndex((item) => skuId === item.skuId)
                this.cartList.splice(idx, 1)
            }
        },
        // 单选功能
        async singleCheck(skuId, selected) {
            // 通过skuId找到要修改的那一项 然后把它的selected修改为传过来的selected
            const item = this.cartList.find((item) => item.skuId === skuId)
            item.selected = selected
        },
        // 全选功能action
        async allCheck(selected) {
            // 把cartList中的每一项的selected都设置为当前的全选框状态
            this.cartList.forEach(item => item.selected = selected)
        },
        // 清除购物车
        async clearCart() {
            this.cartList = []
        },
        // 合并购物车
        async mergeCarts() {
            await mergeCart(this.cartList.map(item => {
                return {
                    skuId: item.skuId,
                    selected: item.selected,
                    count: item.count,
                }
            }))
            this.updateNewList()
        },
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});