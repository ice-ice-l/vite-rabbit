import { defineStore } from 'pinia';
import { getCategoryFilter, getSubCategory } from '@/apis/subCategory'

export const useSubCategoryStore = defineStore('subCategory', {
    state: () => ({
        categoryInfo: {}, // 二级分类
        subCategoryGoods: [],  // 二级分类商品列表数据
        isLoading: false, // 是否在请求数据
        disabled: false, // 是否停止请求数据
    }),
    getters: {
        // doubleCount: (state) => state.count * 2,
    },
    actions: {
        async getCategory(id) {
            let res = await getCategoryFilter(id)
            // console.log("getCategoryFilter ~ res:", res)
            this.categoryInfo = res.result
        },
        async getSubCategoryData(data) {
            this.isLoading = true
            let res = await getSubCategory(data)
            // console.log("getSubCategoryList ~ res:", res)
            // 加载完毕 停止监听
            if (res.result.items.length === 0) {
                this.disabled = true
            }
            this.subCategoryGoods = [...this.subCategoryGoods, ...res.result.items]
            this.isLoading = false
        },
        changeData(payload) {
            this.disabled = payload.disabled
            this.subCategoryGoods = payload.list
        }
    },
    persist: true, // 设置持久化
    // 单独设置存储位置
    // persist: {
    //   storage: window.localStorage,
    // },
});