const Const = {
    NET: {
        URL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net'
    },

    // 热榜
    HOT_TITLEMAP: {
        1: { title: '24小时热榜', label: 'time' },
        2: { title: '周热榜', label: 'week' },
    },
    HOT_TITLE: {
        TIME: 1,  // 24小时热榜
        WEEK: 2,   // 周热榜
    }
}

export default Const