// axios基础的封装
import axios from 'axios'
import Const from './const'
import { useUserStore } from '@/stores/user'
import router from '@/router'

const httpInstance = axios.create({
    baseURL: Const.NET.URL,
    timeout: 5000
})


// 拦截器

// axios请求拦截器
httpInstance.interceptors.request.use(config => {
    // 1. 从pinia获取token数据
    const userStore = useUserStore()
    // 2. 按照后端的要求拼接token数据
    const token = userStore.userInfo.token
    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }
    return config
}, e => Promise.reject(e))

// axios响应式拦截器
httpInstance.interceptors.response.use(res => res.data, e => {
    // 统一错误提示
    ElMessage({
        type: 'warning',
        message: e.response.data.message
    })

    // 401 token失效处理
    if (e.response.status === 401) {
        const userStore = useUserStore()
        // 1. 清除用户数据
        userStore.logOut()
        // 2. 跳转到登录页
        router.push('/login')
    }

    return Promise.reject(e)
})


export default httpInstance