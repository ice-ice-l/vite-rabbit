
/**
 * 是否是空对象
 */
function isEmptyObj(obj) {
    for (var key in obj) {
        return false;
    }
    return true;
};

export default {
    isEmptyObj
}