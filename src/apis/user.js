import httpInstance from "@/utils/http";

/**
 * @description: 登录
 * @param {*} account 账号 
 * @param {*} password 密码 
 * @return {*}
 */
export const login = ({ account, password }) => {
    return httpInstance({
        url: 'login',
        method: 'POST',
        data: {
            account,
            password
        }
    })
}

/**
 * @description: 猜你喜欢接口
 * @param {*} limit limit
 * @return {*}
 */
export const getLikeList = ({ limit = 4 }) => {
    return httpInstance({
        url: 'goods/relevant',
        params: {
            limit
        }
    })
}

/**
 * @description: 用户订单接口
 * @param {*} params params 
 * @return {*}
 */
export const getUserOrder = (params) => {
    return httpInstance({
        url: 'member/order',
        method: 'GET',
        params
    })
}


