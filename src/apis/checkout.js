import httpInstance from "@/utils/http";

/**
 * @description: 获取结算信息
 * @return {*}
 */
export const getCheckoutInfo = () => {
    return httpInstance({
        url: 'member/order/pre'
    })
}

