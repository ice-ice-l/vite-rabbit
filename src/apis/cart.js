import httpInstance from "@/utils/http";

/**
 * @description: 加入购物车
 * @param {*} skuId skuId 
 * @param {*} count 数量
 * @return {*}
 */
export const insertCart = ({ skuId, count }) => {
    return httpInstance({
        url: 'member/cart',
        method: 'POST',
        data: {
            skuId,
            count
        }
    })
}

/**
 * @description: 删除购物车
 * @param {*} ids ids 
 * @return {*}
 */
export const deleteCart = (ids) => {
    return httpInstance({
        url: 'member/cart',
        method: 'DELETE',
        data: {
            ids
        }
    })
}

/**
 * @description: 获取最新的购物车列表
 * @return {*}
 */
export const findNewCartList = () => {
    return httpInstance({
        url: 'member/cart'
    })
}

/**
 * @description: 合并购物车
 * @param {*} data 数据
 * @return {*}
 */
export const mergeCart = (data) => {
    return httpInstance({
        url: 'member/cart/merge',
        method: 'POST',
        data
    })
}
