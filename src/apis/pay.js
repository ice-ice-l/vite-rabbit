import httpInstance from "@/utils/http";

/**
 * @description: 创建订单
 * @param {*} data data 
 * @return {*}
 */
export const createOrder = (data) => {
    return httpInstance({
        url: 'member/order',
        method: 'POST',
        data
    })
}

/**
 * @description: 获取订单信息
 * @param {*} id id 
 * @return {*}
 */
export const getOrder = (id) => {
    return httpInstance({
        url: `member/order/${id}`
    })
}

