import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

// 引入字体图标
import '@/assets/iconfont/iconfont.css'

// 引入路由实例
import router from "./router"
// 引入pinia 实例
import pinia from './stores';

// 引入样式
import '@/styles/common.scss'

// 全局指令注册
import directives from '@/directives'

// 引入方法
import utils from '@/utils/utils'

// 引入文件
import Const from './utils/const'

// 引入全局组件插件
import { componentPlugin } from '@/components'

const app = createApp(App);

app.use(router).use(pinia).use(componentPlugin)

// 全局方法
app.config.globalProperties.$U = utils;
app.config.globalProperties.$C = Const;

// 指令注册
for (const key in directives) {
    app.use(directives[key])
}

app.mount('#app')
